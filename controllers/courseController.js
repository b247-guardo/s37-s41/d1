const Course = require("../models/Course");
const auth = require("../auth");

// Add courses
module.exports.addCourse = (data) => {

	if (data.isAdmin) {

		let newCourse = new Course ({
		
		name: data.course.name,
		description: data.course.description,
		price: data.course.price
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "New course succesfully created!"
		}};
	});
}
	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	});

	return message.then((value) => {
		return value;
	});
};

// Get all courses
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Get all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	});
};

// Get a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			let message = `Successfully updated course Id - "${reqParams.id}"`;
			return message;
		};
	});
};

// Archive a Course
module.exports.archiveCourse = (reqParams, reqBody) => {

	const isActive = {isActive : reqBody.isActive};

	return Course.findByIdAndUpdate(reqParams.id, isActive).then((archive, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


/*module.exports.isAdmin = (isAdmin) => {

		return Course.findOne(isAdmin).then(result => {
			if (!result) {
				return false;
			} else {
				return true;
			}
		});
	};*/


